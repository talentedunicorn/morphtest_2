import { Injectable } from '@nestjs/common';
import { AxiosAdapter } from './adapters/axios.adapters';

@Injectable()
export class AppService {
  constructor(private readonly axiosAdapter: AxiosAdapter) {}
  getCart(): Promise<any> {
    return this.axiosAdapter.get(`/carts/2`);
  }

  getUser(id: string) {
    return this.axiosAdapter.get(`/users/${id}`);
  }
}
