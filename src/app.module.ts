import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AxiosAdapter } from './adapters/axios.adapters';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, AxiosAdapter],
})
export class AppModule {}
