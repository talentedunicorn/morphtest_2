import { Controller, Get, NotFoundException } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getCart(): Promise<any> {
    try {
      const cart = await this.appService.getCart();
      const userId = cart.userId;

      const user = await this.appService.getUser(userId);
      return {
        ...cart,
        user,
      };
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
