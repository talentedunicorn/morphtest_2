import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class AxiosAdapter {
  baseUri = 'https://dummyjson.com';

  async get(path: string) {
    try {
      const response = await axios.get(`${this.baseUri}${path}`);
      return response.data;
    } catch (e) {
      throw new Error(e);
    }
  }
}
