## Morph test 2

Backend Microservice / REST API test (Beginner/Intermediate)


## Requirements

You are to build a project consist of key elements as below:

1. Create a microservice REST API with routing/controller/version such as: a. Eg: your_domain/cart/api/v1.0/fetch
1. First fetch cart details from https://dummyjson.com/carts/2, then extract from response data to find “userId”
1. Next, fetch user details by “userId” found in step 2 from https://dummyjson.com/users/<userId>. Replace with actual userId found in step 2.
1. Rebuild final json to include cart & user details and return consolidated payload as API response/output.

Requirement:
• Use NodeJS
• This application should be Dockerized into a Dockerfile


## Installation

```bash
$ pnpm install
```

## Running the app

### Docker
```bash
# Build an image using the Dockerfile
$ docker build -t morphtest2 .

# Run a container and map port 3000
$ docker run --name morphtest -dp 3000:3000 morphtest2

# Now you can call GET http://localhost:3000 to get response
$ curl http://localhost:3000
```

### Without Docker

```bash
# development
$ pnpm run start

# watch mode
$ pnpm run start:dev

# production mode
$ pnpm run start:prod
```

## Test

```bash
# unit tests
$ pnpm run test

# e2e tests
$ pnpm run test:e2e

# test coverage
$ pnpm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## License

Nest is [MIT licensed](LICENSE).
